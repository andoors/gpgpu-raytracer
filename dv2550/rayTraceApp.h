#ifndef RAYTRACEAPP_H
#define RAYTRACEAPP_H

#include "stdafx.h"
#include "Shader.h"
#include "Buffer.h"
#include "Camera.h"
#include "Timer.h"

using namespace glm;
using namespace std;

struct KeyData
{
	bool pressed;
};
struct MouseData
{
	int x, y;
	float dx, dy;
	bool lmdown;
	bool rmdown;
	bool moved;
	MouseData()
	{
		x = 0;
		y = 0;
		dx = 0;
		dy = 0;
		lmdown = false;
		moved = false;
	}
};

struct Vertex 
{
	vec3 p;
	float u;
	vec3 n;
	float v;
	vec4 t;
	vec4 b;
	vec4 c = vec4(0,0,0,0);

	Vertex(vec3 position, vec3 normal, float _u, float _v, vec4 _t = vec4(0, 0, 0, 0), vec4 _b = vec4(0, 0, 0, 0))
	{
		p = position;
		n = normal;
		u = _u;
		v = _v;
		t = _t;
		b = _b;
	}
};
struct Object
{
	int vertstart;
	int vertstop;
	int texture;
	int normal;

	Object(int start, int stop, int tex, int norm)
	{
		vertstart = start;
		vertstop = stop;
		texture = tex;
		normal = norm;
	}
};
struct ocTree
{
	vec3 min;
	int start;
	vec3 max;
	int stop;
	vec3 padding;
	int childrenCount;

	ocTree(vec3 _min, vec3 _max, float _start, float _stop)
	{
		min = _min;
		max = _max;
		start = _start;
		stop = _stop;
	}
};
struct PointLight
{
	vec4 Diffuse;
	vec4 Specular;
	vec3 Position;
	float Range;

	PointLight(vec4 diff, vec4 spec, vec3 pos, float rng)
	{
		Diffuse = diff;
		Specular = spec;
		Position = pos;
		Range = rng;
	}
};

struct TestSettings
{
	int wwidth;
	int bounces;
	int lights;

	TestSettings(int ww, int b, int l)
	{
		wwidth = ww;
		bounces = b;
		lights = l;
	}
};

class rayTraceApp
{
public:
	rayTraceApp();
	~rayTraceApp();

	void init(SDL_Window* p_Window);

	void update(float dt);

	void render();

	MouseData	m_MouseData;
	KeyData		m_W, m_A, m_S, m_D;

	void input(SDL_Keycode key, bool up);
	void resize();

	// settings and headeroutputs
	unsigned int nrofBounces;
	unsigned int nrofLights;
	unsigned int nrofVertices;

	// Quit
	bool *m_Quit;
private:
	SDL_Window*		m_Window;
	SDL_GLContext	m_GLContext;

	Camera			m_Camera;

	void updateCamera(Shader p_Shader);

	void initShaders();
	void initBuffers();
	void initTextureArray(int maxwidth, int maxheight);

	void rayCreation(bool clear = true);
	void intersection();
	void colorstep();
	void endDraw();

	void aabbdraw();

	int generateOctree(vector<Vertex>* vertices, int VertStart, vec3, vec3);
	bool vertexInsideAABB(Vertex, vec3, vec3);
	void expandAABB(Vertex, vec3*, vec3*);
	void splitAABB(vec3* ocMin, vec3* ocMax, vec3 min, vec3 max);

	void addObject(int start, int end, int textureMap, int normalMap);
	int loadModel(string file, vec3 offset, float scale, float Yrotation = 0);
	void loadTexture(string file, int place);

	// Shaders
	Shader				m_RayCreationShader;
	Shader				m_IntersectionShader;
	Shader				m_ColorStageShader;
	Shader				m_FullScreenShader;
	Shader				m_LightMovementShader;
	Shader				m_PickingShader;
	Shader				m_AABBIntersectionShader;
	float				time;

	// texture
	GLuint				textureArray;
	// Image buffers
	GLuint				ray_dirbuffer;
	GLuint				ray_posbuffer;
	GLuint				hitbuffer;
	GLuint				output_image;
	// Vertex buffer
	GLuint				Object_buffer;
	GLuint				OcTree_buffer;
	GLuint				vertex_buffer;
	GLuint				light_buffer;

	vector<Vertex>		m_Vertices;
	vector<ocTree>		m_OcTree;
	vector<Object>		m_Objects;
	vector<PointLight>	m_Lights;

	// Window size
	int			m_ClientWidth, m_ClientHeight;

	// TEST CODE
	vector<TestSettings> m_Test;
	int m_TestId;
	ofstream m_TestFile;
};

#endif