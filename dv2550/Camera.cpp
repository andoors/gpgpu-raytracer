#include "Camera.h"

Camera::Camera()
{
	m_Position = glm::vec3(6, 0, 0);
	m_Up = vec3(0, 1, 0);
	m_Look = vec3(0, 0, 0) - m_Position;
	m_Look = normalize(m_Look);
	m_Right = cross(m_Up, m_Look);
	m_Right = normalize(m_Right);
	m_Up = cross(m_Look, m_Right);
}

Camera::Camera(vec3 p_Pos, vec3 p_Up, vec3 p_Target)
{
	m_Position = p_Pos;
	m_Up = p_Up;
	m_Look = p_Target - m_Position;
	m_Look = normalize(m_Look);
	m_Right = cross(m_Up, m_Look);
	m_Right = normalize(m_Right);
	m_Up = cross(m_Look, m_Right);
}

Camera::~Camera(){}

void Camera::update(float dt)
{
}

void Camera::Walk(float p_Dist)
{
	m_Position += m_Look * p_Dist;
}

void Camera::Strafe(float p_Dist)
{

	m_Position += m_Right * p_Dist;

}

void Camera::Ascend(float p_Dist)
{
	m_Position += m_Up * p_Dist;
}

void Camera::Pitch(float p_Angle)
{
	vec3 look = rotate(m_Look, p_Angle, m_Right);
	vec3 up = rotate(m_Up, p_Angle, m_Right);

	m_Look = look;
	m_Look = normalize(m_Look);
	m_Up = up;
	m_Up = normalize(m_Up);
}

void Camera::YawY(float p_Angle)
{
	vec3 look = rotateY(m_Look, p_Angle);
	vec3 right = rotateY(m_Right, p_Angle);
	vec3 up = rotateY(m_Up, p_Angle);

	m_Look = look;
	m_Look = normalize(m_Look);
	m_Right = right;
	m_Right = normalize(m_Right);
	m_Up = up;
	m_Up = normalize(m_Up);
}

void Camera::Yaw(float p_Angle)
{
	vec3 look = m_Look * cosf(p_Angle) + m_Right * sinf(p_Angle);
	vec3 right = m_Right * cosf(p_Angle) - m_Look * sinf(p_Angle);

	m_Look = look;
	m_Look = normalize(m_Look);
	m_Right = right;
	m_Right = normalize(m_Right);
}

void Camera::Spinn(float p_Angle)
{
	vec3 up = m_Up * cosf(p_Angle) + m_Right * sinf(p_Angle);
	vec3 right = m_Right * cosf(p_Angle) - m_Up * sinf(p_Angle);

	m_Up = up;
	m_Up = normalize(m_Up);
	m_Right = right;
	m_Right = normalize(m_Right);
}

void Camera::SetPosition(float p_x, float p_y, float p_z)
{
	m_Position.x = p_x;
	m_Position.y = p_y;
	m_Position.z = p_z;
}

void Camera::SetPosition(vec3 p_Pos)
{
	m_Position = p_Pos;
}

void Camera::setLook(vec3 p_Look)
{
	m_Look = p_Look - m_Position;

	m_Look = normalize(m_Look);
}