#ifndef CAMERA_H
#define CAMERA_H

#include "stdafx.h"
using namespace glm;

class Camera
{
public:
	Camera();
	Camera(vec3 p_Pos, vec3 p_Up, vec3 p_Target);
	~Camera();

	// Move the camera.
	void Strafe(float p_Dist);
	void Walk(float p_Dist);
	void Ascend(float p_Dist);

	// Rotate the camera.
	void Pitch(float p_Angle);
	void Yaw(float p_Angle);
	void YawY(float p_Angle);
	void Spinn(float p_Angle);

	void SetPosition(vec3 p_Pos);
	void SetPosition(float p_x, float p_y, float p_z);

	void setLook(vec3 p_Look);

	void update(float dt);

	vec3 GetPosition() { return m_Position; };
	vec3 GetLook() { return m_Look; };
	vec3 GetUp() { return m_Up; };
	vec3 GetRight() { return m_Right; };

private:

	vec3 m_Position;
	vec3 m_Up;
	vec3 m_Right;
	vec3 m_Look;

	// Frustum properties.
	float m_Near;
	float m_Far;
	float m_Aspect;
	float m_FovY;
	float m_NearWindowHeight;
	float m_FarWindowHeight;
};

#endif
