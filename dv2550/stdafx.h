#ifndef STDAFX_H
#define STDAFX_H

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <map>
#include <limits>
#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
#include <vld.h>

#define SAFE_RELEASE(x)	if(x)	{ (x)->Release();	(x) = 0; }
#define SAFE_DELETE(x)	if(x)	{ delete(x);	(x) = nullptr; }
#define RELEASE(x) { if (x) { x->Release(); x = NULL; } }

#define GLEW_STATIC

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#undef main

// opengl
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

// assimp
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

// texture loader


// SDL Window
#pragma comment(lib, "SDL2.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glew32s.lib")
#pragma comment(lib, "SDL2_mixer.lib")

// ASSIMP MODEL LOADER
#ifdef _DEBUG
#pragma comment(lib, "assimpd.lib")
#else
#pragma comment(lib, "assimp.lib")
#endif
#pragma comment(lib, "zlibstaticd.lib")

#endif
