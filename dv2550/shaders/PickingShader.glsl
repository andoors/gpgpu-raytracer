#version 430 core

// UNIFORMS
uniform ivec2 screenposition;

layout (local_size_x = 16) in;

struct Vertex
{
	vec3 p;
	float u;
	vec3 n;
	float v;
	vec4 t;
	vec4 b;
	vec4 c;
};

layout (rgba32f, binding = 3) uniform image2D ray_dirbuffer;
layout (rgba32f, binding = 4) uniform image2D ray_posbuffer;

layout (std140, binding = 2) buffer VertexBuffer { 	Vertex verts []; };

bool RayVsTriangle(vec3 ray_pos, vec3 ray_dir, Vertex v0, Vertex v1, Vertex v2);

void main(void)
{
	unsigned int pos = gl_GlobalInvocationID.x * 3;

    vec3 ray_d = imageLoad(ray_dirbuffer, screenposition).xyz;
    vec3 ray_o = imageLoad(ray_posbuffer, screenposition).xyz;
	
	if (RayVsTriangle(ray_o,ray_d,verts[pos],verts[pos+1],verts[pos+2]))
	{
		verts[pos].c += vec4(0.1);
		verts[pos+1].c += vec4(0.1);
		verts[pos+2].c += vec4(0.1);
	}
	else
	{
		verts[pos].c = vec4(0);
		verts[pos+1].c = vec4(0);
		verts[pos+2].c = vec4(0);
	}
}




bool RayVsTriangle(vec3 ray_pos, vec3 ray_dir, Vertex v0, Vertex v1, Vertex v2)
{
	vec3 e1 = v1.p - v0.p;
	vec3 e2 = v2.p - v0.p;
	vec3 q = cross(ray_dir, e2);
	float a = dot(e1, q);
	if (a == 0) return false;

	float f = 1/a;
	vec3 s = ray_pos - v0.p;
	float u = f*(dot(s, q));
	if (u < 0 || u > 1) return false;

	vec3 p = cross(s, e1);
	float v = f*(dot(ray_dir, p));
	if (v < 0 || u + v > 1) return false;

	float t = dot(e2, p) * f;

	if (t < 0.0001) return false;
		
	if (t >= 20) return false;

	return true;
};