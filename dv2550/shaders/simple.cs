#version 430 core
// UNIFORMS
uniform vec3 camera_right;
uniform vec3 camera_up;
uniform vec3 camera_position;
uniform vec3 camera_direction;

layout (local_size_x = 16, local_size_y = 16) in;

// IMG
layout (rgba32f) uniform image2D output_image;

void main(void)
{
    float width = gl_WorkGroupSize.x*gl_NumWorkGroups.x;  // pixels across
    float height = gl_WorkGroupSize.y*gl_NumWorkGroups.y;  // pixels high
    float normalized_i = (gl_GlobalInvocationID.x / width) - 0.5;
    float normalized_j = (gl_GlobalInvocationID.y / height) - 0.5;
    vec3 image_point =  normalized_i * camera_right + 
                        normalized_j * camera_up + 
                        camera_direction;
    vec3 ray_direction = image_point;

    vec3 ray_o = camera_position;
    vec3 ray_d = ray_direction;

	imageStore(
        output_image,
        ivec2(gl_GlobalInvocationID.xy),
        vec4(ray_direction.x, ray_direction.y, ray_direction.z, 0.0)
	);

    vec3 s_o = vec3(2,0,0);
    float s_r = 0.05f;

    vec3 l = s_o - ray_o;
	float l2 = dot(ray_d, l);
	if (l2 > 0)
	{
		float l_sq = dot(l, l);
		float r_sq = s_r*s_r;
		if (l_sq > r_sq)
		{
			if(l_sq-l2*l2 <= r_sq) 
			{
				float b = dot(ray_o-s_o, ray_d);
				float c = dot((ray_o-s_o), (ray_o-s_o))-r_sq;
				float h = b*b-c;
				if (h > 0)
				{
					float t1 = -b + sqrt(h);
					float t2 = -b - sqrt(h);
					float t = t1 < t2 ? t1 : t2;
					if (t > 0)
					{

	            imageStore(
                    output_image,
                    ivec2(gl_GlobalInvocationID.xy),
                    vec4(1, 1, 1, 0.0)
	            );

					}
				}
			}
		}
	}
}