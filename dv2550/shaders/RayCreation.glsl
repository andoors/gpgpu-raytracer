#version 430 core
// UNIFORMS
uniform vec3 camera_right;
uniform vec3 camera_up;
uniform vec3 camera_position;
uniform vec3 camera_direction;
uniform bool clear_image;

layout (local_size_x = 16, local_size_y = 16) in;

// IMG
layout (rgba32f, binding = 3) uniform image2D ray_dirbuffer;
layout (rgba32f, binding = 4) uniform image2D ray_posbuffer;

layout (rgba32f, binding = 7) uniform image2D output_image;

void main(void)
{
    float width = gl_WorkGroupSize.x*gl_NumWorkGroups.x;  // pixels across
    float height = gl_WorkGroupSize.y*gl_NumWorkGroups.y;  // pixels high
    float normalized_i = (gl_GlobalInvocationID.x / height) - (width/height)*0.5;
    float normalized_j = (gl_GlobalInvocationID.y / height) - 0.5;
    vec3 image_point =  normalized_i * camera_right + 
                        normalized_j * camera_up + 
                        camera_direction;
    vec3 ray_direction = normalize(image_point);

    vec3 ray_o = camera_position;
    vec3 ray_d = ray_direction;

	imageStore(
        ray_dirbuffer,
        ivec2(gl_GlobalInvocationID.xy),
        vec4(ray_direction.x, ray_direction.y, ray_direction.z, 0.0)
	);

	imageStore(
        ray_posbuffer,
        ivec2(gl_GlobalInvocationID.xy),
        vec4(camera_position.x, camera_position.y, camera_position.z, 0.0)
	);

	if (!clear_image) return;

	imageStore(
        output_image,
        ivec2(gl_GlobalInvocationID.xy),
        vec4(0.0)
	);
}