#version 430 core

//#extension GL_ARB_shading_language_420pack: enable  Use for GLSL versions before 420.

struct OcTree
{
	vec3 _min;
	int start;
	vec3 _max;
	int stop;
	vec3 padding;
	int childrenCount;
};

struct Vertex
{
	vec3 p;
	float u;
	vec3 n;
	float v;
	vec4 t;
	vec4 b;
	vec4 c;

};

// UNIFORMS
layout (local_size_x = 16, local_size_y = 16) in;

// AABB Buffer
//layout (std430, binding = 0) buffer ObjectBuffer { 	Object objs []; };
// AABB Buffer
layout (std430, binding = 1) buffer OcTreeBuffer { 	OcTree ocs []; };
// Vertices
layout (std140, binding = 2) buffer VertexBuffer { 	Vertex verts []; };//layout (std140, binding = 1) buffer AABBBuffer { AABB aabbs []; };
// Point Lights
//layout (std430, binding = 6) buffer PointLightBuffer { 	PointLight lights []; };

// IMG
layout (rgba32f, binding = 3) uniform image2D ray_dirbuffer;
layout (rgba32f, binding = 4) uniform image2D ray_posbuffer;
layout (rgba32f, binding = 5) uniform image2D hitbuffer;
// output 
layout (rgba32f, binding = 7) uniform image2D output_image;

int RayVsOcTree(vec3 ray_pos, vec3 ray_dir);
void RayVsOcAABBs(vec3 ray_pos, vec3 ray_dir);
bool RayVsAABB(vec3 ray_pos, vec3 ray_dir, vec3 pbounds0, vec3 pbounds1);
bool RayVsAABBedge(vec3 ray_pos, vec3 ray_dir, vec3 pbounds0, vec3 pbounds1);

void main(void)
{
    vec3 ray_d = imageLoad(ray_dirbuffer, ivec2(gl_GlobalInvocationID.xy)).xyz;
    vec3 ray_o = imageLoad(ray_posbuffer, ivec2(gl_GlobalInvocationID.xy)).xyz;

	vec3 prev_color		= imageLoad(output_image, ivec2(gl_GlobalInvocationID.xy)).xyz;

	int i = 0;
	// TEST RAY VS TRIANGLE AND SPHERE
	/*for (int i = 0; i < objs.length(); i++)
	{
		RayVsOcTree(ray_o, ray_d, objs[i].oc);
	}*/
	/*for (int j = 0; j < verts.length(); j+=3)
	{
		RayVsTriangle(ray_o,ray_d,verts[j],verts[j+1],verts[j+2], float(j));
	}*/

	int hits = RayVsOcTree(ray_o, ray_d);

	imageStore(
		output_image,
		ivec2(gl_GlobalInvocationID.xy),
		vec4(vec3(hits*0.05f)+prev_color, 0.0)
	);
}

int RayVsOcTree(vec3 ray_pos, vec3 ray_dir)
{
	int hits = 0;
	for (int i = 0; i < ocs.length(); i++)
	{
		// Check collsion
		if (RayVsAABB(ray_pos,ray_dir,ocs[i]._min,ocs[i]._max))
		{
			// IS IT LEAF NODE?
			for (int j = ocs[i].start; j < ocs[i].stop; j+=3)
			{
				return hits;
			} 
			hits++;
		}
		else
			i += ocs[i].childrenCount;
	}
	return hits;
}

bool RayVsAABB(vec3 ray_pos, vec3 ray_dir, vec3 pbounds0, vec3 pbounds1)
{
    vec3 invdir = 1.0f / ray_dir.xyz;
 
    float t1 = (pbounds0.x - ray_pos.x)*invdir.x;
    float t2 = (pbounds1.x - ray_pos.x)*invdir.x;
    float t3 = (pbounds0.y - ray_pos.y)*invdir.y;
    float t4 = (pbounds1.y - ray_pos.y)*invdir.y;
    float t5 = (pbounds0.z - ray_pos.z)*invdir.z;
    float t6 = (pbounds1.z - ray_pos.z)*invdir.z;
 
    float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
    float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));
 
    if(tmax < 0 || tmin > tmax)
        return false;
 
    return true;
}

bool RayVsAABBedge(vec3 ray_pos, vec3 ray_dir, vec3 pbounds0, vec3 pbounds1)
{
    vec3 invdir = 1.0f / ray_dir.xyz;
 
    float t1 = (pbounds0.x - ray_pos.x)*invdir.x;
    float t2 = (pbounds1.x - ray_pos.x)*invdir.x;
    float t3 = (pbounds0.y - ray_pos.y)*invdir.y;
    float t4 = (pbounds1.y - ray_pos.y)*invdir.y;
    float t5 = (pbounds0.z - ray_pos.z)*invdir.z;
    float t6 = (pbounds1.z - ray_pos.z)*invdir.z;
 
    float tmin = max(max(min(t1, t2), min(t3, t4)), min(t5, t6));
    float tmax = min(min(max(t1, t2), max(t3, t4)), max(t5, t6));
 
    if(tmax < 0 || tmin > tmax)
        return false;
 
    return false;
}