#version 430 core
// UNIFORMS
uniform float time;

layout (local_size_x = 16) in;

struct PointLight
{ 
	vec4 Diffuse;
	vec4 Specular;
	vec3 Position;
	float Range;
};

layout (std430, binding = 6) buffer PointLightBuffer { 	PointLight lights []; };

void main(void)
{
	float x = sin(time+gl_GlobalInvocationID.x*3);
	float y = cos(time+gl_GlobalInvocationID.x);
	float z = cos(time+gl_GlobalInvocationID.x*10);
	lights[gl_GlobalInvocationID.x].Position = vec3(x,y,z)+vec3(0,0.5,0);
}