#include <iostream>
#include "stdafx.h"
#include "Timer.h"
#include "rayTraceApp.h"

int main(int argc, char** argv)
{
	SDL_Window*		g_Win;
	Timer			g_Timer(true);
	rayTraceApp		g_App;

	bool			g_Quit;

	// WINDOW SETTINGS
	unsigned int	g_Flags = SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN;
	const char*		g_Caption = "dv2550";
	int				g_PosX = 100;
	int				g_PosY = 100;
	int				g_SizeX = 256*4;
	int				g_SizeY = 144*4;

	if (SDL_Init(SDL_INIT_EVERYTHING) == -1){
		std::cout << SDL_GetError() << std::endl;
		return 1;
	}

	// PLATFORM SPECIFIC CODE
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	g_Flags += SDL_WINDOW_OPENGL;

	// INIT WINDOW
	g_Win = SDL_CreateWindow(g_Caption, g_PosX, g_PosY, g_SizeX, g_SizeY, g_Flags);
	if (g_Win == NULL){
		std::cout << SDL_GetError() << std::endl;
		return 1;
	}
	g_App.init(g_Win);

	MouseData* md = &g_App.m_MouseData;

	char title[255];
	g_Quit = false;
	g_App.m_Quit = &g_Quit;
	while (!g_Quit)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_KEYDOWN:
				/*Example on how to get what key caused the event*/
				if (event.key.keysym.sym == SDLK_ESCAPE)
					g_Quit = true;
				else
					g_App.input(event.key.keysym.sym, false);
				break;

			case SDL_KEYUP:
				g_App.input(event.key.keysym.sym, true);
				break;

			case SDL_MOUSEMOTION:
				int x, y;
				SDL_GetMouseState(&x, &y);
				md->dx = x - md->x;
				md->dy = y - md->y;
				md->x = x;
				md->y = y;
				md->moved = true;
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button == SDL_BUTTON_LEFT)
					md->lmdown = true;
				if (event.button.button == SDL_BUTTON_RIGHT)
					md->rmdown = true;
				break;

			case SDL_MOUSEBUTTONUP:
				if (event.button.button == SDL_BUTTON_LEFT)
					md->lmdown = false;
				if (event.button.button == SDL_BUTTON_RIGHT)
					md->rmdown = false;
				break;

			case SDL_WINDOWEVENT:
				switch (event.window.event) 
				{
				case SDL_WINDOWEVENT_RESIZED:
					g_App.resize();
					SDL_GetWindowSize(g_Win, &g_SizeX, &g_SizeY);
					break;
				}
				break;
				/*
			case SDL_QUIT:
				SDL_Quit();
				break;
				*/
			default:

				break;
			}
		}
		SDL_PumpEvents();

		float ms = g_Timer.Elapsed().count();
		sprintf_s(title, sizeof(title), "%dx%d - V: %u - B: %u - L: %u - FPS: %d", g_SizeX, g_SizeY, g_App.nrofVertices, g_App.nrofBounces, g_App.nrofLights, (int)(1000.0f / (float)ms));
		SDL_SetWindowTitle(g_Win, title);

		g_Timer.Reset();

		g_App.update(ms*0.001f);
		g_App.render();

	}   // End while

	SDL_DestroyWindow(g_Win);
	SDL_Quit();
	return 0;
}
