#include "rayTraceApp.h"

//#define _TEST
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

rayTraceApp::rayTraceApp()
{	
	nrofBounces = 1;
	nrofLights = 2;
	nrofVertices = 0;
}

rayTraceApp::~rayTraceApp()
{
	glDeleteBuffers(1, &Object_buffer);
	glDeleteBuffers(1, &OcTree_buffer);
	glDeleteBuffers(1, &vertex_buffer);
	glDeleteBuffers(1, &light_buffer);

	glDeleteBuffers(1, &ray_dirbuffer);
	glDeleteBuffers(1, &ray_posbuffer);
	glDeleteBuffers(1, &hitbuffer);
	glDeleteBuffers(1, &output_image);

	glDeleteTextures(10, &textureArray);

#ifdef _TEST
	m_TestFile.close();
#endif
}

void rayTraceApp::init(SDL_Window* p_Window)
{
	m_Window = p_Window;
	m_GLContext = SDL_GL_CreateContext(m_Window);
	SDL_GetWindowSize(m_Window, &m_ClientWidth, &m_ClientHeight);
	glewInit();
	SDL_GL_SetSwapInterval(0);
	
	initShaders();
	initTextureArray(1024, 1024);

	// INIT OBJECTS
	int start, stop;
	loadTexture("objs/nerzhul/diffuse.png", 0);
	loadTexture("objs/nerzhul/normalpixel.png", 1);
	start = m_Vertices.size();
	stop = loadModel("objs/nerzhul/nerzhulmask.3ds", vec3(0, 1.f, 0), 0.5f);
	addObject(start, stop, 0, 1);

	loadTexture("objs/box/box.png", 2);
	loadTexture("objs/box/normal.png", 3);
	start = m_Vertices.size();
	stop = loadModel("objs/box/box.obj", vec3(0, 1.5f, 0), 1.5f);
	addObject(start, stop, 2, 3);
	
	nrofVertices = m_Vertices.size();

	// Test octree
	vector<bool> vertused;
	for (int i = 0; i < m_Vertices.size(); i++)
		vertused.push_back(false);
	for (int i = 0; i < m_OcTree.size(); i++)
	{
		for (int j = m_OcTree[i].start; j < m_OcTree[i].stop; j+=3)
		{
			vertused[j] = vertexInsideAABB(m_Vertices[j], m_OcTree[i].min, m_OcTree[i].max);
			vertused[j + 1] = vertexInsideAABB(m_Vertices[j+1], m_OcTree[i].min, m_OcTree[i].max);
			vertused[j + 2] = vertexInsideAABB(m_Vertices[j+2], m_OcTree[i].min, m_OcTree[i].max);
		}
	}
	for (int i = 0; i < vertused.size(); i++)
		if (vertused[i] == false)
			printf_s("some vertex wasn't in octree");

	// INIT LIGHTS
	for (int i = 0; i < 10; i++)
	{
		float x = 1.0f;// (rand() % 50)*0.02;
		float y = 0.9f;// (rand() % 50)*0.02;
		float z = 0.8f;// (rand() % 50)*0.02;
		m_Lights.push_back(PointLight(vec4(x, y, z, 1), vec4(x, y, z, 1), vec3(1, 0, 0), 0.5f));
	}

	initBuffers();

	// AUTO TEST CODE
#ifdef _TEST
	m_TestFile = ofstream("test.csv");;
	if (!m_TestFile.is_open())
		printf_s("failed to open file.");

	m_TestFile << "ID,Vertices,Bounces,Lights,Rays,Resolution,Thread Group Size,Ray Creation Stage,Intersection Stage,Color Stage\n";

	m_TestId = 0;
	for (int i = 1; i < 5; i++)
		for (int j = 1; j < 6; j++)
			for (int k = 1; k < 6; k++)
				m_Test.push_back(TestSettings(256*i, j, k));
#endif

}

void rayTraceApp::initShaders()
{
	// RayCreation Shader
	ShaderInfo shaderInfo[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/RayCreation.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_RayCreationShader.init(shaderInfo);

	// Intersection Shader
	ShaderInfo shaderInfo2[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/Intersection.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_IntersectionShader.init(shaderInfo2);

	// ColorStage Shader
	ShaderInfo shaderInfo3[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/ColorStage.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_ColorStageShader.init(shaderInfo3);

	// LightMovement Shader
	ShaderInfo shaderInfo4[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/LightMovement.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_LightMovementShader.init(shaderInfo4);

	// Picking Shader
	ShaderInfo shaderInfo5[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/PickingShader.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_PickingShader.init(shaderInfo5);

	// Picking Shader
	ShaderInfo shaderInfo6[] =
	{
		{ GL_COMPUTE_SHADER, "shaders/AABBIntersection.glsl" },
		{ GL_NONE, NULL },
		{ GL_NONE, NULL }
	};
	m_AABBIntersectionShader.init(shaderInfo6);

	// Full Screen Quad Shader
	ShaderInfo shaderInfo9[] =
	{
		{ GL_VERTEX_SHADER, "shaders/fullscreen.vs" },
		{ GL_GEOMETRY_SHADER, "shaders/fullscreen.gs" },
		{ GL_FRAGMENT_SHADER, "shaders/fullscreen.ps" }
	};
	m_FullScreenShader.init(shaderInfo9);
}
void rayTraceApp::initBuffers()
{
	// -----------------
	// TEXTURE BUFFERS
	// -----------------
	// Ray direction buffer
	glGenTextures(1, &ray_dirbuffer);
	glBindTexture(GL_TEXTURE_2D, ray_dirbuffer);
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA32F, m_ClientWidth, m_ClientHeight);
	// Ray position buffer
	glGenTextures(1, &ray_posbuffer);
	glBindTexture(GL_TEXTURE_2D, ray_posbuffer);
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA32F, m_ClientWidth, m_ClientHeight);
	// Hit result buffer
	glGenTextures(1, &hitbuffer);
	glBindTexture(GL_TEXTURE_2D, hitbuffer);
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA32F, m_ClientWidth, m_ClientHeight);
	// Ray direction buffer
	glGenTextures(1, &output_image);
	glBindTexture(GL_TEXTURE_2D, output_image);
	glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA32F, m_ClientWidth, m_ClientHeight);

	// -----------------
	// DATA BUFFERS
	// -----------------
	// Create vertex buffer
	glGenBuffers(1, &vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_Vertices.size(), &m_Vertices[0], GL_STATIC_DRAW);
	// Create aabb buffer
	glGenBuffers(1, &OcTree_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, OcTree_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ocTree) * m_OcTree.size(), &m_OcTree[0], GL_STATIC_DRAW);
	// Create aabb buffer
	glGenBuffers(1, &Object_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, Object_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Object) * m_Objects.size(), &m_Objects[0], GL_STATIC_DRAW);
	// Create light buffer
	glGenBuffers(1, &light_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, light_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PointLight) * m_Lights.size(), &m_Lights[0], GL_STATIC_DRAW);

	// -----------------
	// BIND BUFFERS
	// -----------------
	// Bind AABB buffer
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, Object_buffer);
	// Bind AABB buffer
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, OcTree_buffer);
	// Bind vertex buffer
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, vertex_buffer);
	// Bind light buffer
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, light_buffer);
	// Bind texture buffers
	glBindImageTexture(3, ray_dirbuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	glBindImageTexture(4, ray_posbuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	glBindImageTexture(5, hitbuffer, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
	glBindImageTexture(7, output_image, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
}
void rayTraceApp::initTextureArray(int maxwidth, int maxheight)
{
	//Generate an array texture
	glGenTextures(1, &textureArray);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);

	//Create storage for the texture. (100 layers of 1x1 texels)
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, maxwidth, maxheight, 10);
}

void rayTraceApp::addObject(int start, int end, int textureMap, int normalMap)
{
	m_Objects.push_back(Object(start, end, textureMap, normalMap));
}

int rayTraceApp::loadModel(string file, vec3 offset, float _scale, float Yrotation)
{
	int start = m_Vertices.size();

	mat4 modelMatrix = mat4(1);
	mat4 rotation = mat4(1);
	mat4 scale = glm::scale(vec3(_scale));
	mat4 translation = glm::translate(offset);

	modelMatrix = translation * rotation * scale;

	vector<Vertex> verts;

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(file, aiProcessPreset_TargetRealtime_Fast);//aiProcessPreset_TargetRealtime_Fast has the configs you'll need
	aiMesh *mesh = scene->mMeshes[0];

	aiVector3D min = aiVector3D(10e10f, 10e10f, 10e10f);
	aiVector3D max = aiVector3D(-10e10f, -10e10f, -10e10f);
	for (unsigned int i = 0; i<mesh->mNumFaces; i++)
	{
		const aiFace& face = mesh->mFaces[i];

		for (int j = 0; j<3; j++)
		{
			//aiVector3D uv = mesh->mTextureCoords[0][face.mIndices[j]];
			aiVector3D n = mesh->mNormals[face.mIndices[j]];
			aiVector3D tc = mesh->mTextureCoords[0][face.mIndices[j]];
			aiVector3D p = mesh->mVertices[face.mIndices[j]];
			aiVector3D t = mesh->mTangents[face.mIndices[j]];
			aiVector3D b = mesh->mBitangents[face.mIndices[j]];

			min.x = std::min(p.x, min.x);
			min.y = std::min(p.y, min.y);
			min.z = std::min(p.z, min.z);

			max.x = std::max(p.x, max.x);
			max.y = std::max(p.y, max.y);
			max.z = std::max(p.z, max.z);

			verts.push_back(Vertex(vec3(vec4(p.x, p.y, p.z, 1)*modelMatrix), vec3(vec4(n.x, n.y, n.z, 1)*modelMatrix), tc.x, tc.y, vec4(t.x, t.y, t.z, 0)*modelMatrix, vec4(b.x, b.y, b.z, 0)*modelMatrix));
		}
	}
	vec3 AABBmin = vec3(vec4(min.x, min.y, min.z, 0)*modelMatrix);
	vec3 AABBmax = vec3(vec4(max.x, max.y, max.z, 0)*modelMatrix);

	int nrofverts = verts.size();
	int oc = generateOctree(&verts, start, AABBmin, AABBmax);

	if (verts.size() != nrofverts)
		printf_s("something failed while generating Octree\n nrofVerts: %d verts in Octree: %d\n", nrofverts, verts.size());

	for (int i = 0; i < verts.size(); i++)
		m_Vertices.push_back(verts[i]);

	return start + nrofverts;
}

int rayTraceApp::generateOctree(vector<Vertex>* vertices, int VertStart, vec3 min, vec3 max)
{
	// reserv a place for me last in tree
	int treeplace = m_OcTree.size();
	int childrenCount = 0;
	ocTree oc = ocTree(min, max, 0, 0);
	m_OcTree.push_back(oc);

	if (vertices->size() < 120)
	{ 
		oc.start = VertStart;
		oc.stop = VertStart + vertices->size() - 1;
		oc.childrenCount = childrenCount;

		m_OcTree[treeplace] = oc;
		return childrenCount;
	}

	// new Min Max
	vec3 ocMin[8];
	vec3 ocMax[8];
	vector<Vertex> ocVerts;
	splitAABB(&ocMin[0], &ocMax[0], min, max);


	int _start = VertStart;
	vector<Vertex> ocVertsTemp;
	for (int i = 0; i < 8; i++)
	{
		ocVerts.clear();
		vec3 _min = ocMin[i];
		vec3 _max = ocMax[i];
		for (int j = 0; j < vertices->size(); j += 3)
		{
			int inside = 0;
			bool notinside[3];
			// Check if triangle is inside AABB
			for (int k = 0; k < 3; k++)
			{ 
				if (vertexInsideAABB((*vertices)[j + k], ocMin[i], ocMax[i]))
				{
					inside++;
					notinside[k] = true;
				}
				else
					notinside[k] = false;
			}

			if (inside > 0)
			{
				for (int k = 0; k < 3; k++)
				{
					if (notinside[k] == false)
					{
						if (!vertexInsideAABB((*vertices)[j + k], _min, _max))
							expandAABB((*vertices)[j + k], &_min, &_max); 
					}
				}

				ocVerts.push_back((*vertices)[j]);
				ocVerts.push_back((*vertices)[j + 1]);
				ocVerts.push_back((*vertices)[j + 2]);
				vertices->erase(vertices->begin() + j);
				vertices->erase(vertices->begin() + j);
				vertices->erase(vertices->begin() + j);
				j -= 3;
			}
		}
		childrenCount += generateOctree(&ocVerts, VertStart, _min, _max);
		VertStart += ocVerts.size();
		for (int j = 0; j < ocVerts.size(); j++)
		{
			ocVertsTemp.push_back(ocVerts[j]);
		}
	}
	// make 8 children
	childrenCount += 8;
	oc.childrenCount = childrenCount;

	if (vertices->size() > 0)
		printf_s("wtf?\n");

	for (int j = 0; j < ocVertsTemp.size(); j++)
	{
		vertices->push_back(ocVertsTemp[j]);
	}

	m_OcTree[treeplace] = oc;
	return childrenCount;
}
bool rayTraceApp::vertexInsideAABB(Vertex ver, vec3 min, vec3 max)
{
	if (ver.p.x > max.x) return false;
	if (ver.p.x < min.x) return false;
	if (ver.p.y > max.y) return false;
	if (ver.p.y < min.y) return false;
	if (ver.p.z > max.z) return false;
	if (ver.p.z < min.z) return false;
	return true;
}
void rayTraceApp::expandAABB(Vertex ver, vec3* min, vec3* max)
{
	if (ver.p.x > max->x) max->x = ver.p.x;
	if (ver.p.x < min->x) min->x = ver.p.x;
	if (ver.p.y > max->y) max->y = ver.p.y;
	if (ver.p.y < min->y) min->y = ver.p.y;
	if (ver.p.z > max->z) max->z = ver.p.z;
	if (ver.p.z < min->z) min->z = ver.p.z;
}
void rayTraceApp::splitAABB(vec3* ocMin, vec3* ocMax, vec3 min, vec3 max)
{
	vec3 halfdiff = (max - min) * 0.5f;
	ocMin[0] = min;
	ocMax[0] = ocMin[0] + halfdiff;
	ocMin[1] = min + vec3(0, 0, halfdiff.z);
	ocMax[1] = ocMin[1] + halfdiff;
	ocMin[2] = min + vec3(halfdiff.x, 0, halfdiff.z);
	ocMax[2] = ocMin[2] + halfdiff;
	ocMin[3] = min + vec3(halfdiff.x, 0, 0);
	ocMax[3] = ocMin[3] + halfdiff;
	ocMax[4] = max;
	ocMin[4] = ocMax[4] - halfdiff;
	ocMax[5] = max - vec3(0, 0, halfdiff.z);
	ocMin[5] = ocMax[5] - halfdiff;
	ocMax[6] = max - vec3(halfdiff.x, 0, halfdiff.z);
	ocMin[6] = ocMax[6] - halfdiff;
	ocMax[7] = max - vec3(halfdiff.x, 0, 0);
	ocMin[7] = ocMax[7] - halfdiff;
}


void rayTraceApp::loadTexture(string file, int place)
{
	int w;
	int h;
	int comp;
	unsigned char* image = stbi_load(file.c_str(), &w, &h, &comp, STBI_rgb_alpha);
	
	if (image == nullptr)
		return;

	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, place, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, image);
	
	stbi_image_free(image);
	
}

void rayTraceApp::update(float dt)
{
	time += dt;
#ifdef _TEST
	time = 0;
#endif
	// Use LightMovement Shader
	glUseProgram(m_LightMovementShader.GetShaderProgram());
	glUniform1f(glGetUniformLocation(m_LightMovementShader.GetShaderProgram(), "time"), time);
	glDispatchCompute(m_Lights.size(), 1, 1);


	if (m_MouseData.rmdown && m_MouseData.moved)
	{
		m_Camera.YawY(m_MouseData.dx * 0.1f);
		m_Camera.Pitch(m_MouseData.dy * 0.1f);
		m_MouseData.moved = false;
	}
	if (m_W.pressed)
	{
		m_Camera.Walk(dt * 5);
	}
	if (m_S.pressed)
	{
		m_Camera.Walk(-dt * 5);
	}
	if (m_A.pressed)
	{
		m_Camera.Strafe(-dt * 5);
	}
	if (m_D.pressed)
	{
		m_Camera.Strafe(dt * 5);
	}
}

void rayTraceApp::render()
{
#ifdef _TEST
	SDL_SetWindowSize(m_Window, m_Test[m_TestId].wwidth, m_ClientHeight);
	resize();
	nrofBounces = m_Test[m_TestId].bounces;
	nrofLights = m_Test[m_TestId].lights;
	m_TestId++;

	if (m_TestId >= m_Test.size())
		(*m_Quit) = true;

	GLuint startQuery, endQuery;
	GLuint64 startTime, endTime;
	GLint stopTimerAvailibale;
	float raycreationTime = 0;
	float intersectionTime = 0;
	float colorstepTime = 0;

	glGenQueries(1, &startQuery);
	glGenQueries(1, &endQuery);
#endif

	glBindTexture(GL_TEXTURE_2D_ARRAY, textureArray);

#ifdef _TEST
	glQueryCounter(startQuery, GL_TIMESTAMP);
#endif
	rayCreation();
#ifdef _TEST
	glQueryCounter(endQuery, GL_TIMESTAMP);
	stopTimerAvailibale = 0;
	while (!stopTimerAvailibale)
	{
		glGetQueryObjectiv(endQuery, GL_QUERY_RESULT_AVAILABLE, &stopTimerAvailibale);
	}
	glGetQueryObjectui64v(startQuery, GL_QUERY_RESULT, &startTime);
	glGetQueryObjectui64v(endQuery, GL_QUERY_RESULT, &endTime);
	raycreationTime = (endTime - startTime) / 1000000.0;
#endif

	// PICKING
	if (m_MouseData.lmdown) {
		glUseProgram(m_PickingShader.GetShaderProgram());
		glUniform2i(glGetUniformLocation(m_PickingShader.GetShaderProgram(), "screenposition"), m_MouseData.x, m_ClientHeight - m_MouseData.y);
		glDispatchCompute(int(m_Vertices.size() / 3), 1, 1);
	}

	for (int i = 0; i < nrofBounces; i++)
	{
#ifdef _TEST
		glQueryCounter(startQuery, GL_TIMESTAMP);
#endif
		intersection();
#ifdef _TEST
		glQueryCounter(endQuery, GL_TIMESTAMP);
		stopTimerAvailibale = 0;
		while (!stopTimerAvailibale)
		{
			glGetQueryObjectiv(endQuery, GL_QUERY_RESULT_AVAILABLE, &stopTimerAvailibale);
		}
		glGetQueryObjectui64v(startQuery, GL_QUERY_RESULT, &startTime);
		glGetQueryObjectui64v(endQuery, GL_QUERY_RESULT, &endTime);
		intersectionTime += (endTime - startTime) / 1000000.0;
#endif
#ifdef _TEST
		glQueryCounter(startQuery, GL_TIMESTAMP);
#endif
		colorstep();
#ifdef _TEST
		glQueryCounter(endQuery, GL_TIMESTAMP);
		stopTimerAvailibale = 0;
		while (!stopTimerAvailibale)
		{
			glGetQueryObjectiv(endQuery, GL_QUERY_RESULT_AVAILABLE, &stopTimerAvailibale);
		}
		glGetQueryObjectui64v(startQuery, GL_QUERY_RESULT, &startTime);
		glGetQueryObjectui64v(endQuery, GL_QUERY_RESULT, &endTime);
		colorstepTime += (endTime - startTime) / 1000000.0;
#endif
	}
	

#ifdef _TEST
	m_TestFile << m_TestId;
	m_TestFile << ",";
	m_TestFile << nrofVertices;
	m_TestFile << ",";
	m_TestFile << nrofBounces;
	m_TestFile << ",";
	m_TestFile << nrofLights;
	m_TestFile << ",";
	m_TestFile << m_ClientWidth*m_ClientHeight;
	m_TestFile << ",";
	m_TestFile << m_ClientWidth;
	m_TestFile << "x";
	m_TestFile << m_ClientHeight;
	m_TestFile << ",";
	m_TestFile << "16x16";
	m_TestFile << ",";
	m_TestFile << raycreationTime;
	m_TestFile << ",";
	m_TestFile << intersectionTime;
	m_TestFile << ",";
	m_TestFile << colorstepTime;
	m_TestFile << "\n";
#endif

	colorstep();

	rayCreation(false);
	//aabbdraw();

	glBindTexture(GL_TEXTURE_2D, output_image);

	endDraw();	
}

void rayTraceApp::input(SDL_Keycode key, bool up)
{
	if (key == SDLK_a)
		m_A.pressed = !up;

	if (key == SDLK_d)
		m_D.pressed = !up;

	if (key == SDLK_w)
		m_W.pressed = !up;

	if (key == SDLK_s)
		m_S.pressed = !up;

	if (key == SDLK_l && up)
		if (nrofLights < m_Lights.size()) nrofLights++;
	if (key == SDLK_k && up)
		if (nrofLights > 0) nrofLights--;

	if (key == SDLK_o && up)
		nrofBounces++;
	if (key == SDLK_i && up)
		if (nrofBounces > 0) nrofBounces--;
}
void rayTraceApp::resize()
{
	int x, y;
	SDL_GetWindowSize(m_Window, &x, &y);
	x = x / 256;
	if (x == 0) x = 1;
	m_ClientWidth = x * 256;
	m_ClientHeight = x * 144;
	SDL_SetWindowSize(m_Window, m_ClientWidth, m_ClientHeight);
}

void rayTraceApp::rayCreation(bool clear)
{
	// Use RayCreationShader
	glUseProgram(m_RayCreationShader.GetShaderProgram());
	// Set Camera in RayCreationShader
	glUniform3f(glGetUniformLocation(m_RayCreationShader.GetShaderProgram(), "camera_right"), m_Camera.GetRight().x, m_Camera.GetRight().y, m_Camera.GetRight().z);
	glUniform3f(glGetUniformLocation(m_RayCreationShader.GetShaderProgram(), "camera_up"), m_Camera.GetUp().x, m_Camera.GetUp().y, m_Camera.GetUp().z);
	glUniform3f(glGetUniformLocation(m_RayCreationShader.GetShaderProgram(), "camera_position"), m_Camera.GetPosition().x, m_Camera.GetPosition().y, m_Camera.GetPosition().z);
	glUniform3f(glGetUniformLocation(m_RayCreationShader.GetShaderProgram(), "camera_direction"), m_Camera.GetLook().x, m_Camera.GetLook().y, m_Camera.GetLook().z);

	glUniform1i(glGetUniformLocation(m_RayCreationShader.GetShaderProgram(), "clear_image"), clear);

	// RUN!
	glDispatchCompute(m_ClientWidth * 0.0625, m_ClientHeight * 0.0625, 1); // 1/16 = 0.0625
}

void rayTraceApp::intersection()
{
	// Use IntersectionShader
	glUseProgram(m_IntersectionShader.GetShaderProgram());
	// Run program
	glDispatchCompute(m_ClientWidth * 0.0625, m_ClientHeight * 0.0625, 1); // 1/16 = 0.0625
}

void rayTraceApp::colorstep()
{
	// Use ColorStageShader
	glUseProgram(m_ColorStageShader.GetShaderProgram());
	// Set Uniforms
	glUniform1ui(glGetUniformLocation(m_ColorStageShader.GetShaderProgram(), "nrofLights"), nrofLights);
	// Run program
	glDispatchCompute(m_ClientWidth * 0.0625, m_ClientHeight * 0.0625, 1); // 1/16 = 0.0625
}

void rayTraceApp::aabbdraw()
{
	// Use AABBdrawShader
	glUseProgram(m_AABBIntersectionShader.GetShaderProgram());
	// Run program
	glDispatchCompute(m_ClientWidth * 0.0625, m_ClientHeight * 0.0625, 1); // 1/16 = 0.0625
}

void rayTraceApp::endDraw()
{
	// Clear, select the rendering program and draw a full screen quad	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(m_FullScreenShader.GetShaderProgram());
	glDrawArrays(GL_POINTS, 0, 1);
	// Swap in the new buffer
	SDL_GL_SwapWindow(m_Window);
}

void rayTraceApp::updateCamera(Shader p_Shader)
{
	glUseProgram(p_Shader.GetShaderProgram());
	glUniform3f(glGetUniformLocation(p_Shader.GetShaderProgram(), "camera_right"), m_Camera.GetRight().x, m_Camera.GetRight().y, m_Camera.GetRight().z);
	glUniform3f(glGetUniformLocation(p_Shader.GetShaderProgram(), "camera_up"), m_Camera.GetUp().x, m_Camera.GetUp().y, m_Camera.GetUp().z);
	glUniform3f(glGetUniformLocation(p_Shader.GetShaderProgram(), "camera_position"), m_Camera.GetPosition().x, m_Camera.GetPosition().y, m_Camera.GetPosition().z);
	glUniform3f(glGetUniformLocation(p_Shader.GetShaderProgram(), "camera_direction"), m_Camera.GetLook().x, m_Camera.GetLook().y, m_Camera.GetLook().z);
}